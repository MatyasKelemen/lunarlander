"use strict";

let rotationSoundPlaying = false;

class LineBreakTransformer {
    constructor() {
        this.container = "";
    }

    transform(chunk, controller) {
        this.container += chunk;
        const lines = this.container.split("\r\n");
        this.container = lines.pop();
        lines.forEach((line) => controller.enqueue(line));
    }

    flush(controller) {
        controller.enqueue(this.container);
    }
}

async function openSerialPort(callback) {
    if ("serial" in navigator) {
        const port = await navigator.serial.requestPort();
        await port.open({ baudRate: 19200 });
        callback(port);
    } else {
        console.warn("The Web Serial API is not supported");
    }
}

async function readSerial(port) {
    const lineReader = port.readable
        .pipeThrough(new TextDecoderStream())
        .pipeThrough(new TransformStream(new LineBreakTransformer()))
        .getReader();
    try {
        while (port.readable) {
            const { value, done } = await lineReader.read();
            if (done) {
                console.log("The reader has been canceled");
                break;
            }
            if (value) {
                controlLM(value);
            }
        }
    } catch (error) {
        console.error(error);
    } finally {
        lineReader.releaseLock();
    }
    await port.close();
}

function playRotationSound(duration) {
    return new Promise((resolve, reject) => {
        if (rotationSoundPlaying) { 
            return reject("The previous playback has not finished yet");
        }
        rotationSound.play().then(() => {
            rotationSoundPlaying = true;
            resolve("Playback has started");
            for (let i = 0; i < 8; i++) {
                const x = Number.parseFloat(rotationSound.volume - 0.1).toFixed(1);
                rotationSound.volume = x <= 0 ? 0 : x;
            }
            setTimeout(() => {
                rotationSound.pause();
                rotationSound.volume = 1;
                rotationSoundPlaying = false;
            }, duration);
        }).catch(error => {
            console.error(error);
        });
    });
}

function controlLM(command) {
    if (gameState == PLAYING && command) {
        const values = command.split("/");
        if (values[0] == -0.5 || values[0] == 0.5) {
            lander.rotate(values[0]);
            playRotationSound(250).catch(error => {
                console.error(error);
            });
        }
        if (values[1] >= 0) {
            lander.thrust(Math.abs(values[1]));
        }
    }
}
