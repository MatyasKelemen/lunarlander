// Lunar Lander

int leftRotation = 10;
int rightRotation = 11;
int halfThrust = 12;
int fullThrust = 13;

double attitude = 0.00;
double thrust = 0.00;

void setup() {
  Serial.begin(19200);
  pinMode(leftRotation, INPUT);
  pinMode(rightRotation, INPUT);
  pinMode(halfThrust, INPUT);
  pinMode(fullThrust, INPUT);
};

void loop() {
  if (digitalRead(leftRotation) == HIGH) {
    attitude = -0.50;
  } else if (digitalRead(rightRotation) == HIGH) {
    attitude = 0.50;
  } else {
    attitude = 0.00;
  };

  if (digitalRead(fullThrust) == HIGH) {
    thrust = 1.00;
  } else if (digitalRead(halfThrust) == HIGH) {
    thrust = 0.50;
  } else {
    thrust = 0.00;
  };

  if (Serial.availableForWrite() > (sizeof(attitude) + sizeof(thrust))) {
    Serial.print(attitude, 1);
    Serial.print("/");
    Serial.println(thrust, 1);
  };
};