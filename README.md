# Lunar Lander
An Arduino and Web Serial API based controller for a retro game where you have to land the Apollo Lunar Module on the Moon. \
The game was forked from the following repository: https://github.com/sebleedelisle/apollolander.

### Dev. requirements:
- [Git](https://git-scm.com/)
- [Chrome](https://www.google.com/chrome/)

### Installation of the Dev. Environment:
    $> yarn [install]
        
### Running the game locally:
- Connect the controller
- Open a Web Browser with Serial API support
- Open file: index.html
- Press key: S
- Select the Serial port of the controller
- Check the console for debug messages

### Circuit diagram:
![Hardware Debounce](./arduino/circuit.png)
